package cargarregistros;

import cargarregistros.gui.InterfazRegistro;
import monitor.Registro;
import monitor.Registros;
import monitor.Sintomas;

public class CargarRegistros {

    private final Sintomas sintomas;
    private final ArchivoRegistro archivo;

    public CargarRegistros(Sintomas sintomas) {
        this.sintomas = sintomas;
        archivo = new ArchivoRegistro();
    }

    private void cargarSintoma() {
        InterfazRegistro interRegistro = new InterfazRegistro(sintomas);
    }

    public Registro getRegistro() {
        cargarSintoma();
        return archivo.ultimoRegistro();
    }
    public Registros getRegistros() {
        cargarSintoma();
        return archivo.getRegistros();
    }

}
