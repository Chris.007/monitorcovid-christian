package cargarregistros.gui;

import cargarregistros.ArchivoRegistro;
import monitor.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.util.*;


public class InterfazRegistro extends JFrame{
    private final JFrame frameAux;
    private final JPanel panel;
    private final JList<String> listaRegistros;
    private final JList<String> listaSintomas;
    private final JScrollPane scrollListaRegistro,scrollSintomas;
    private final JLabel tituloRegistro, tituloMisRegistros;
    private final JButton btnRegistrarSintomas;
    private final JButton btnTerminar;
    private final JTable tabla;
    private final JScrollPane scroll;
    private final Sintomas sintomas;
    private final Date fecha;
    private final ArchivoRegistro archivo;

    public InterfazRegistro(Sintomas sintomas){
        this.sintomas = sintomas;
        frameAux = this;
        panel = new JPanel();
        btnRegistrarSintomas = new JButton("Registrar Sintomas");
        btnTerminar = new JButton("Terminar");
        tituloRegistro = new JLabel("REGISTRAR MIS SINTOMAS");
        tituloMisRegistros = new JLabel("MIS REGISTROS");
        listaRegistros = new JList<>();
        listaSintomas = new JList<>();
        scrollListaRegistro = new JScrollPane(listaRegistros);
        scrollSintomas = new JScrollPane(listaSintomas);
        tabla = new JTable();
        scroll = new JScrollPane(tabla);
        archivo = new ArchivoRegistro();
        fecha = new Date();
        iniciarComponentes();
    }
    private void iniciarComponentes(){
        addDatosTabla();
        cargarListaRegistros();
        addComponentesPanel();
        this.setSize(800,650);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setTitle("REGISTROS");
        this.setResizable(false);
        panel.setLayout(null);
        panel.setBackground(Color.DARK_GRAY);
        this.setContentPane(panel);
        btnListener();
    }

    private void addComponentesPanel(){
        JLabel tituloFecha = new JLabel("FECHA: ");
        tituloFecha.setBounds(65,80,180,100);
        tituloFecha.setForeground(Color.white);
        DateFormat formato = DateFormat.getDateInstance(DateFormat.FULL);
        JLabel fecha_actual = new JLabel(formato.format(fecha));
        fecha_actual.setBounds(115,80,180,100);
        fecha_actual.setForeground(Color.white);

        JLabel tituloHora = new JLabel("HORA: ");
        tituloHora.setBounds(65,100,180,100);
        tituloHora.setForeground(Color.white);
        DateFormat formatoHora = DateFormat.getTimeInstance(DateFormat.SHORT);
        JLabel hora_actual = new JLabel(formatoHora.format(fecha));
        hora_actual.setBounds(115,100,180,100);
        hora_actual.setForeground(Color.white);


        JLabel TituloFase = new JLabel(miFase());
        TituloFase.setBounds(110,80,180,30);
        TituloFase.setForeground(Color.orange);
        TituloFase.setFont(new Font("Tahoma", Font.PLAIN, 18));



        tituloRegistro.setBounds(280,20,330,20);
        tituloRegistro.setFont(new Font("Tahoma", Font.PLAIN, 18));
        tituloRegistro.setForeground(Color.white);
        tituloMisRegistros.setBounds(330,340,330,20);
        tituloMisRegistros.setFont(new Font("Tahoma", Font.PLAIN, 18));
        tituloMisRegistros.setForeground(Color.white);
        scrollListaRegistro.setBounds(45,380,200,150);
        scrollSintomas.setBounds(265,380,480,150);
        scroll.setBounds(350,60,400,250);
        btnRegistrarSintomas.setBounds(100,230,160,30);
        btnTerminar.setBounds(320,550,160,30);

        panel.add(TituloFase);
        panel.add(scroll);
        panel.add(scrollSintomas);
        panel.add(scrollListaRegistro);
        panel.add(btnRegistrarSintomas);
        panel.add(btnTerminar);
        panel.add(tituloHora);
        panel.add(hora_actual);
        panel.add(tituloFecha);
        panel.add(fecha_actual);
        panel.add(tituloRegistro);
        panel.add(tituloMisRegistros);
    }
    private String miFase(){
        DatosFase archivoFase = new DatosFase();
        Fase fase = archivoFase.leerDatosFase();
        String res =fase.getNombre();
        if (res.equals("PrimeraFase")){
            res = "PRIMERA FASE";
        }else {
            res = "SEGUNDA FASE";
        }
        return res;
    }

    private void cargarListaRegistros() {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        Stack<Registro> registros = archivo.getRegistrosOrdenados();
        for (Registro r:registros) {
            String fecha = r.getFecha().toString();
            listModel.addElement(fecha);
        }
        listaRegistros.setModel(listModel);
    }

    private void addDatosTabla(){
        DefaultTableModel datos = new DefaultTableModel();
        datos.addColumn("Sintomas");
        datos.addColumn("Mis Selecionados");

        for (Sintoma sintoma : getSintomasSort()) {
            datos.addRow(new Object[]{sintoma});
        }
        TableRowSorter<TableModel> sort = new TableRowSorter<>(datos);
        tabla.setRowSorter(sort);
        tabla.setModel(datos);
        //Colocar Check
        TableColumn tc = tabla.getColumnModel().getColumn(1);
        tc.setCellEditor(tabla.getDefaultEditor(Boolean.class));
        tc.setCellRenderer(tabla.getDefaultRenderer(Boolean.class));
    }

    public TreeSet<Sintoma> getSintomasSort(){
        TreeSet<Sintoma> datos = new TreeSet<>();
        for (Sintoma sintoma:sintomas) {
            datos.add(sintoma);
        }
        return datos;
    }

    private boolean estaMarcadoTabla(){
        boolean res = false;
        for (int i = 0 ;i<tabla.getRowCount();i++){
            if (estaMarcado(i)) {
                res = true;
                break;
            }
        }
        return res;
    }

    private void btnListener(){
        btnRegistrarSintomas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(estaMarcadoTabla() && !archivo.existeFecha()){
                    guardarSintomasMarcados();
                    addDatosTabla();
                    cargarListaRegistros();
                }

            }
        });

        btnTerminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cerrarVentana();
            }
        });

        listaRegistros.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                String miFechaSelecionada = listaRegistros.getSelectedValue();
                if (miFechaSelecionada != null) {
                    mostrarMisSintomasSeleccionados(miFechaSelecionada);
                }
            }
        });

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we){
                cerrarVentana();
            }
        });

        synchronized(frameAux){
            try{
                frameAux.wait();
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    private void mostrarMisSintomasSeleccionados(String llave){
        Map<String,Sintomas> res = archivo.getRegistrosDate();
        DefaultListModel<String> lista = new DefaultListModel<String>();
        Sintomas sintomas =(Sintomas)res.get(llave);
        for (Sintoma sintoma : sintomas) {
            lista.addElement(sintoma.toString());
        }
        listaSintomas.setModel(lista);
    }

    private void cerrarVentana(){
        synchronized(frameAux) {
            frameAux.notify();
        }
        frameAux.setVisible(false);
        frameAux.dispose();
    }

    private void guardarSintomasMarcados(){
        Sintomas misSintomas = new Sintomas();
        for (int i = 0 ;i<tabla.getRowCount();i++){
            if(estaMarcado(i)){
                Sintoma sintoma =(Sintoma)tabla.getValueAt(i,0);
                misSintomas.add(sintoma);
            }
        }
        Registro nuevoRegistro = new Registro(new Date(),misSintomas);
        archivo.guardarRegistro(nuevoRegistro);
    }

    private boolean estaMarcado(int row) {
        boolean res = false;
        if (tabla.getValueAt(row, 1) != null) {
            res = (Boolean)tabla.getValueAt(row, 1);
        }
        return res;
    }

}
