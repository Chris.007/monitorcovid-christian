package cargarregistros;
import monitor.Registro;
import monitor.Registros;
import monitor.Sintomas;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import static java.util.Collections.reverse;

public class ArchivoRegistro {

    private final String dirArchivoRegistro = getDirArchivo();

    public ArchivoRegistro(){
        verificarArchivo();
    }

    private void verificarArchivo(){
        File archivoRegistro = new File(this.dirArchivoRegistro);
        if(!archivoRegistro.exists()){
            try {
                archivoRegistro.createNewFile();
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private String getDirArchivo(){
        File archivo = new File(".");
        String slash = System.getProperty("file.separator");
        String ruta ="";
        try {
            ruta = archivo.getCanonicalPath();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        boolean esJar = true;
        String rutaJar = ruta + slash + "registros.txt"; // JAR

        File file = new File(ruta);
        String [] lista = file.list();
        assert lista != null;
        for (String cad:lista) {
            if (cad.equals("src")){
                ruta = "./src/cargarregistros/registros.txt"; // EDITOR
                esJar = false;
                break;
            }
            if (cad.equals("monitor")){
                ruta = "./cargarregistros/registros.txt"; // TERMINAL
                esJar = false;
                break;
            }
        }
        return (esJar) ? rutaJar : ruta;
    }

    public Stack<Registro> getRegistrosOrdenados(){
        Stack<Registro> res = new Stack<>();
        for (Registro r:getRegistros()) {
            res.push(r);
        }
        reverse(res);
        return res ;
    }

    public SortedMap<String, Sintomas> getRegistrosDate(){
        SortedMap<String, Sintomas> res = new TreeMap<>();
        for (Registro registro : getRegistros()) {
            String fecha = registro.getFecha().toString();
            Sintomas sintomas = registro.getSintomas();
            res.put(fecha, sintomas);
        }
        return res;
    }


    public Registros getRegistros(){
        Registros res = new Registros();
        ObjectInputStream file;
        try {
            file = new ObjectInputStream(new FileInputStream(this.dirArchivoRegistro));
            res = (Registros)file.readObject();
            file.close();
        } catch (Exception ignored) {
        }
        return res;
    }

    public void guardarRegistro(Registro registro){
        try {
            Registros registros = getRegistros();
            FileOutputStream file = new FileOutputStream(dirArchivoRegistro);
            ObjectOutputStream agregar = new ObjectOutputStream(file);
            registros.push(registro);
            agregar.writeObject(registros);
            file.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public boolean existeFecha() {
        boolean res = false;
        try {
            Date fechaActual = new Date();
            Registros registros = getRegistros();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            String miFechaActual = formato.format(fechaActual); //ignorando horas
            Date mifecha = formato.parse(miFechaActual); // sin horas
            for (Registro r:registros) {
                String miFechaRegistro = formato.format(r.getFecha());
                Date fechaRegitro = formato.parse(miFechaRegistro);
                if (mifecha.compareTo(fechaRegitro)==0){
                    res = true;
                    break;
                }
            }
        }catch (ParseException ignored){
        }
        return res;
    }

    /**VERIFICAR*/
    public Registro ultimoRegistro(){
        Registro res ;
        if (getRegistros().isEmpty()){
            //System.out.println("ESTA VACIO ARCHIVO REGISTRO");
            res = null;
        }else{
            res = getRegistros().peek();
        }
        return res;
    }

}
