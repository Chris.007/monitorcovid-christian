package cargarsintomas;
import monitor.Sintomas;
import java.io.Serializable;

public class CargarSintomas implements Serializable {
    private Sintomas sintomas;
    private final ArchivoSintoma archivo = new ArchivoSintoma();

    public CargarSintomas() {
        cargarSintomas();
    }

    private void cargarSintomas() {
        new InterfazSintoma();
    }

    public Sintomas getSintomas() {
        sintomas = archivo.getSintomasFile();
        return sintomas;
    }
}
    