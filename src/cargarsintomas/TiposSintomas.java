package cargarsintomas;
import monitor.Sintoma;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class TiposSintomas {

    public List<String> listaDeSintomas(){
        List<String> listaClasesPaquete;
        if (estaComprimido()){
            listaClasesPaquete = tiposSintomasJar();
        }else{
            listaClasesPaquete = tiposSintomasCmd();
        }
        return listaClasesPaquete;
    }

    private boolean estaComprimido(){
        boolean res = false;
        File file = new File(".");
        String [] lista = file.list();
        assert lista != null;
        for (String s : lista) {
            if (s.equals("home.jar")) {
                res = true;
                break;
            }
        }
        return res;
    }

    private List<String> tiposSintomasCmd() {
        List<String> listaClasesPaquete = new ArrayList<>();
        File[] classes = this.obtenerArchivosPaquete();
        Class<Sintoma> sintomaClass = Sintoma.class;

        for (File clase : classes) {
            String nombreClase = clase.getName().split("\\.")[0];
            try {
                Class.forName("sintomas." + nombreClase).asSubclass(sintomaClass);
                listaClasesPaquete.add(nombreClase);
            } catch (Exception ignored) {
            }
        }

        return listaClasesPaquete;
    }

    private File[] obtenerArchivosPaquete() {
        File res = null;
        URL url;
        try {
            for(Enumeration urls = Thread.currentThread().getContextClassLoader().getResources("sintomas"); urls.hasMoreElements(); res = new File(url.getFile())) {
                url = (URL)urls.nextElement();
            }
        } catch (IOException ignored) {
        }
        assert res != null;
        return res.listFiles();
    }

    private List<String> tiposSintomasJar(){
        List<String> lista = new ArrayList<>();
        try {
            ZipInputStream Zip = new ZipInputStream(new FileInputStream("./home.jar"));
            ZipEntry entrar = Zip.getNextEntry();
            while (entrar != null){
                if (!entrar.isDirectory() && entrar.getName().endsWith(".class")) {
                    String nombre = entrar.getName().replace('/', '.');
                    String [] tipos = nombre.split("\\.");
                    String evaluar = tipos[0];
                    if (evaluar.equals("sintomas")){
                        lista.add(tipos[1]);
                    }
                }
                entrar = Zip.getNextEntry();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return lista;
    }
}
