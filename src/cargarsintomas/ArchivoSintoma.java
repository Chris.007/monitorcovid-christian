package cargarsintomas;
import monitor.Sintoma;
import monitor.Sintomas;
import java.io.*;
import java.lang.reflect.Constructor;
import java.util.TreeSet;

public class ArchivoSintoma {

    private final String dirArchivo = getDirArchivo();

    public ArchivoSintoma() {
        existeArchivo();
    }

    private void existeArchivo(){
        File miArchivo = new File(this.dirArchivo);
        if(!miArchivo.exists()){
            try {
                miArchivo.createNewFile();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

    private String getDirArchivo(){
        File archivo = new File(".");
        String slash = System.getProperty("file.separator");
        String ruta ="";
        try {
            ruta = archivo.getCanonicalPath();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        boolean esJar = true;
        String rutaJar = ruta + slash + "sintomas.txt"; // JAR

        File file = new File(ruta);
        String [] lista = file.list();
        assert lista != null;
        for (String cad:lista) {
            if (cad.equals("src")){
                ruta = "./src/cargarsintomas/sintomas.txt"; // EDITOR
                esJar = false;
                break;
            }
            if (cad.equals("monitor")){
                ruta = "./cargarsintomas/sintomas.txt"; // TERMINAL
                esJar = false;
                break;
            }
        }
        return (esJar) ? rutaJar : ruta;
    }

    public TreeSet<Sintoma> getSintomasSort(){
        TreeSet<Sintoma> datos = new TreeSet<>();
        Sintomas sintomas = getSintomasFile();
        for (Sintoma sintoma:sintomas) {
            datos.add(sintoma);
        }
        return datos;
    }

    public Sintomas getSintomasFile(){
        Sintomas sintomas = new Sintomas();
        try{
            FileInputStream file = new FileInputStream(this.dirArchivo);
            ObjectInputStream leer = new ObjectInputStream(file);
            sintomas = (Sintomas) leer.readObject();
            file.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return sintomas;
    }

    public void agregarNuevoSintoma(String dirTipoSintoma,String nombreSintoma){
        Sintoma sintoma = null;
        try {
            Class<?> claseSintoma = Class.forName(dirTipoSintoma);
            Constructor c = claseSintoma.getConstructor(new Class[]{String.class});
            sintoma =(Sintoma)(c.newInstance(new Object[]{nombreSintoma}));
            agregarSintomaArchivo(sintoma);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void agregarSintomaArchivo(Sintoma sintoma){
        try{
            Sintomas sintomas = this.getSintomasFile();
            FileOutputStream file = new FileOutputStream(this.dirArchivo);
            ObjectOutputStream ingresar = new ObjectOutputStream(file);
            sintomas.add(sintoma);
            ingresar.writeObject(sintomas);
            file.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

}