
package cargarsintomas;
import monitor.Sintoma;
import monitor.Sintomas;

import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Validador {

    public boolean estaCorrectoNuevoSintoma(String dato){
        boolean res = false;
        if(dato != null && !dato.strip().equals("")){
            res = validarPasos(dato) && !tieneNombresRepetidos(dato);
        }
        return res;
    }

    //EXPRECIONES REGULARES
    //([a-z1-9]*\\s[a-z1-9]*)*
    private boolean validarPasos(String dato){
        Pattern rango = Pattern.compile("([A-Z1-9]\\s{0,3})*");//patron
        Matcher esValida = rango.matcher(dato);
        return esValida.matches();
    }

    private boolean tieneNombresRepetidos(String dato){
        boolean res = false;
        Sintomas sintomas = new ArchivoSintoma().getSintomasFile();
        Iterator lista = sintomas.iterator();

        while (lista.hasNext()){
            Sintoma sintoma = (Sintoma)lista.next();
            if(sintoma.toString().equals(dato)){
                res = true;
                break;
            }
        }
        return res;
    }
}
