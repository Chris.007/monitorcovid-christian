package cargarsintomas;

import monitor.Sintoma;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.TreeSet;

public class InterfazSintoma extends JFrame {

    private final JLabel titulo,importancia,nombreSintoma;
    private final JButton btnAgregarSintoma;
    private final JButton btnTerminar;
    private final JPanel panel;
    private final JTextArea sintomaText;
    private final JComboBox<String> listaSintomas;
    private final TiposSintomas tipos;
    private final ArchivoSintoma archivo;
    private final Validador validar;
    private final JTable tabla;
    private final JScrollPane scroll;
    private final JFrame frameSincro;

    public InterfazSintoma(){
        frameSincro = this;
        panel = new JPanel();
        tipos = new TiposSintomas();
        titulo = new JLabel("AGREGAR SINTOMAS");
        importancia = new JLabel("Nivel De Importancia");
        nombreSintoma = new JLabel("Nombre Sintoma");
        btnAgregarSintoma = new JButton("Agregar Sintoma");
        btnTerminar = new JButton("Terminar");
        sintomaText = new JTextArea();
        listaSintomas= new JComboBox<>();
        archivo = new ArchivoSintoma();
        validar = new Validador();
        tabla = new JTable();
        scroll = new JScrollPane(tabla);
        iniciarComponentes();
    }
    private void iniciarComponentes(){
        cargarDatosTabla();
        addComponentesPanel();
        agregarBtnPanel();
        this.setSize(800,450);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setTitle("Monitoreo Covid-19");
        panel.setBackground(Color.DARK_GRAY);
        panel.setLayout(null);
        this.add(panel);
        btnListener();
    }


    private void agregarBtnPanel(){
        btnAgregarSintoma.setBounds(100,240,140,30);
        btnTerminar.setBounds(330,325,140,30);
        sintomaText.setBounds(100,190,140,20);
        listaSintomas.setBounds(100,140,140,20);

        for (String tipoSintoma:tipos.listaDeSintomas()) {
            listaSintomas.addItem(tipoSintoma);
        }
        panel.add(btnTerminar);
        panel.add(listaSintomas);
        panel.add(btnAgregarSintoma);
        panel.add(sintomaText);
    }

    private void addComponentesPanel(){
        titulo.setBounds(310,30,190,50);
        titulo.setFont(new Font("Serif", Font.PLAIN, 18));
        titulo.setForeground(Color.white);

        importancia.setBounds(100,100,190,50);
        importancia.setFont(new Font("Serif",Font.PLAIN,14));
        importancia.setForeground(Color.white);

        nombreSintoma.setBounds(100,150,190,50);
        nombreSintoma.setFont(new Font("Serif",Font.PLAIN,14));
        nombreSintoma.setForeground(Color.white);

        scroll.setBounds(350,90,400,200);

        panel.add(scroll);
        panel.add(importancia);
        panel.add(titulo);
        panel.add(nombreSintoma);
    }

    private void btnListener(){
        btnAgregarSintoma.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String texto = sintomaText.getText().toUpperCase();
                if(validar.estaCorrectoNuevoSintoma(texto)){
                    String dirTipoSintoma  = "sintomas"+"."+listaSintomas.getSelectedItem();
                    archivo.agregarNuevoSintoma(dirTipoSintoma,texto);
                    cargarDatosTabla();
                    sintomaText.setText("");
                }else{
                    JOptionPane.showMessageDialog(null,"ERROR AL AGREGAR SINTOMA..!!!");
                }
            }
        });

        btnTerminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cerrarVentana();
            }
        });

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we){
                cerrarVentana();
            }
        });

        synchronized(frameSincro){
            try{
                frameSincro.wait();
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
        }

    }

    private void cerrarVentana(){
        synchronized(frameSincro) {
            frameSincro.notify();
        }
        frameSincro.setVisible(false);
        frameSincro.dispose();
    }

    private void cargarDatosTabla(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Nombre Sintomas");
        model.addColumn("Tipo Sintomas");
        TreeSet<Sintoma> sintomas = archivo.getSintomasSort();
        TableRowSorter<TableModel> sort = new TableRowSorter<>(model);
        tabla.setRowSorter(sort);

        for (Sintoma sintoma : sintomas) {
            String nombreSintoma = sintoma.getClass().getName();//sintoma.PrimeraFase
            String[] dividirCad = nombreSintoma.split("\\.");
            String nombreClass = dividirCad[1];
            model.addRow(new Object[]{sintoma.toString(), nombreClass});
        }
        tabla.setModel(model);
    }

}
