package diagnosticos;
import monitor.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DiagnosticoPorFase extends FuncionDiagnostico {

    private final Sintomas sintomasRegistrados;
    private final DatosFase archivoFase;
    private final Map<Date,Integer> primeraFase;
    private final Map<Date,Integer> segundaFase;
    private final Recomendacion recomendacion;

    public DiagnosticoPorFase(Sintomas sintomas){
        super(sintomas);
        primeraFase = new HashMap<>();
        segundaFase = new HashMap<>();
        sintomasRegistrados = sintomas;
        archivoFase = new DatosFase();
        recomendacion = new Recomendacion();
    }

    @Override
    public int diagnostico(Registros registros) {
        int res = 0;
        Fase miFase = archivoFase.leerDatosFase();
        String nombre = miFase.getNombre();
        if(!registros.isEmpty()){
            if(miFase.getDia()==0){
                miFase.setDia(11);
                archivoFase.guardarDatosFase(miFase);
            }
            if(miFase.getDia()== 20){
                //System.out.println("fase2 ");
                miFase.setNombre("SegundaFase");
                archivoFase.guardarDatosFase(miFase);
            }
            if (nombre.equals("PrimeraFase")){
                evaluarPrimeraFase(registros);
            }else{
                evaluarSegundaFase(registros);
            }
        }else {
            System.out.println("NO REALIZO NINGUN REGISTROS");
        }

        return res;
    }

    private void evaluarPrimeraFase(Registros registros){
        int dia = archivoFase.leerDatosFase().getDia();
        switch (dia){
            case 11://1
                guardarNuevoRegistro(registros,12);
                recomendacion.informacion(1,12);
                break;
            case 12://2
                validar(registros,1,13);
                recomendacion.informacion(2,13);
                break;
            case 13://3
                validar(registros,2,20);
                recomendacion.informacion(3,20);
                break;
        }
    }
    private void evaluarSegundaFase(Registros registros){
        int miDia = archivoFase.leerDatosFase().getDia();
        switch (miDia){
            case 20://4
                validar(registros,0,21); //2
                recomendacion.informacion(4,21);
                break;
            case 21://5
                validar(registros,1,22); //2
                recomendacion.informacion(5,22);
                break;
            case 22://6
                validar(registros,2,23); //2
                recomendacion.informacion(6,23);
                break;
            case 23://7
                validar(registros,3,24); //2
                recomendacion.informacion(7,24);
                break;
            case 24://8
                validar(registros,4,11); //2
                recomendacion.informacion(8,11);
                Fase nuevo =archivoFase.leerDatosFase();
                nuevo.setNombre("Primera");
                archivoFase.guardarDatosFase(nuevo);
                break;
        }

    }

    public void validar(Registros registros,int diasAverificar,int nuevoDia){
        if(agregoNuevoRegistro(registros,diasAverificar)){
            if(verificarDiasAnteriores(registros,diasAverificar)){

                if (esValidoPorcentajePorDia(registros.peek())){
                    guardarNuevoRegistro(registros,nuevoDia);
                }else{
                    reiniciarFaseDia();
                }
            }else{
                reiniciarFaseDia();
            }

        }else{
        }
    }
    private boolean verificarDiasAnteriores(Registros registros,int dias){
        boolean res = false;
        int diasEvaluados = 0;
        LinkedList<Registro> lista = colaRegistros(registros);
        lista.removeLast();
        String miFase = archivoFase.leerDatosFase().getNombre();                                        //0         3

        while(!lista.isEmpty() && (diasEvaluados < dias)){
            Registro registro = lista.removeLast();
            if (esValidoPorcentajePorDia(registro)){

                if (miFase.equals("PrimeraFase")){
                    primeraFase.put(registro.getFecha(),valorPocentaje(registro)); //nuevoDia
                    res = true;
                }else{
                    segundaFase.put(registro.getFecha(),valorPocentaje(registro)); //nuevoDia
                    res = true;
                }
            }else{
                res = false;
                break;
            }
            diasEvaluados = diasEvaluados + 1;
        }
        if (miFase.equals("PrimeraFase")){
            res = primeraFase.size() == dias;
        }else{
            res = segundaFase.size() == dias;
        }
        return res;
    }
        // R , 3
    public boolean agregoNuevoRegistro(Registros registros, int diaAnterior){
        boolean res = false;
        if (existeFecha(new Date(), registros)){
            if(verificarDiasAnteriores(registros,diaAnterior)){
                res = true;
            }
        }
        return res;
    }

    public void reiniciarFaseDia(){
        Fase miFase = archivoFase.leerDatosFase();
        String mifase = miFase.getNombre();
        if(mifase.equals("PrimeraFase")){
            miFase.setDia(11);
        }else {
            miFase.setDia(20);
        }
        archivoFase.guardarDatosFase(miFase);
    }

    private int valorPocentaje(Registro registro){
        String miFase = archivoFase.leerDatosFase().getNombre();
        int misSintomas = cantidadSintomas(registro);
        int totalSintomas = (miFase.equals("PrimeraFase"))? cantSintomasPrimeraFase():cantSintomasTotal();
        int porcentaje = (misSintomas*100)/ totalSintomas;
        return porcentaje;
    }

    private void guardarNuevoRegistro(Registros registros,int dia){ // REGISTROS, 12
        if (existeFecha(new Date(),registros)){
            Registro registro = registros.peek();
            modificarFase(registro,dia);
        }
    }

    public void modificarFase(Registro registro,int dia){
        if (esValidoPorcentajePorDia(registro)){
            Fase nuevo = archivoFase.leerDatosFase();
            nuevo.setDia(dia);
            if (dia == 20){
                nuevo.setNombre("SegundaFase");
            }
            archivoFase.guardarDatosFase(nuevo);
        }
    }

    public boolean esValidoPorcentajePorDia(Registro registro){
        boolean res= false;
        String miFase = archivoFase.leerDatosFase().getNombre();
        int misSintomas = cantidadSintomas(registro);
        int totalSintomas = (miFase.equals("PrimeraFase"))? cantSintomasPrimeraFase():cantSintomasTotal();
        int porcentaje = (misSintomas*100)/ totalSintomas;
        if (porcentaje >= 50){
            res = true;
        }
        return res;
    }

    public int cantSintomasPrimeraFase(){
        int res = 0;
        for (Sintoma s:sintomasRegistrados) {
            String nombre = s.getClass().getName().split("\\.")[1];
            if (nombre.equals("PrimeraFase")){
                res = res +1;
            }
        }
        return res;
    }
    public int cantSintomasTotal(){
        int res = 0;
        for (Sintoma s:sintomasRegistrados) {
            res = res +1;
        }
        return res;
    }

    private int cantidadSintomas(Registro registro){
        int cantidad = 0;
        Sintomas sintomas = registro.getSintomas();
        for (Sintoma s: sintomas) {
            cantidad = cantidad +1;
        }
        return cantidad;
    }

    public boolean existeFecha(Date fechaActual, Registros registros) {
        boolean res = false;
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            String miFechaActual = formato.format(fechaActual); //ignorando horas
            Date mifecha = formato.parse(miFechaActual);        // sin horas

            for (Registro r:registros) {
                String miFechaRegistro = formato.format(r.getFecha());
                Date fechaRegitro = formato.parse(miFechaRegistro);

                if (mifecha.compareTo(fechaRegitro)==0){
                    res = true;
                    break;
                }
            }
        }catch (ParseException ignored){
        }
        return res;
    }

    public LinkedList<Registro> colaRegistros(Registros registros){
        LinkedList<Registro> lista = new LinkedList<>();
        for (Registro r:registros) {
            lista.add(r);
        }
        return lista;
    }

}
