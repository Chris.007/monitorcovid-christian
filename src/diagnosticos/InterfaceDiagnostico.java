package diagnosticos;

import javax.swing.*;
import java.awt.*;

public class InterfaceDiagnostico extends JFrame {
    private final JPanel panel;

    public InterfaceDiagnostico(){
        panel = new JPanel();
    }

    private void addComponentesPanel(){
        JLabel titulo = new JLabel("ROCOMENDACIONES");
        titulo.setBounds(310,20,180,30);
        titulo.setFont(new Font("Serif", Font.PLAIN, 18));
        titulo.setForeground(Color.white);
        panel.add(titulo);
    }

    private void componentes(){
        this.setSize(800,450);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setTitle("RECOMENDACION");
        this.setResizable(false);
        panel.setLayout(null);
        panel.setBackground(Color.DARK_GRAY);
        this.setContentPane(panel);

    }
    public void mostrarVentana(int resultadoDiagnostico){
        //System.out.println("DEBE RELIZAR UNA PRUEBA");
        addComponentesPanel();
        componentes();

    }

}
