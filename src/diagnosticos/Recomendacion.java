package diagnosticos;

import monitor.DatosFase;
import monitor.Fase;

import java.util.Scanner;

public class Recomendacion {

    public void informacion(int dia,int miDia){
        DatosFase archivo = new DatosFase();
        Fase miFase = archivo.leerDatosFase();
        if (miFase.getDia() == miDia){   //11 20
            informacionFase(dia);
        }else{
            informacionInicial(dia);
        }
    }
    public void informacionInicial(int dia){
        DatosFase archivo = new DatosFase();
        Fase fase = archivo.leerDatosFase();
        System.out.println("FASE : " + fase.getNombre());
        System.out.println("DIA: " + dia);
        System.out.println("\t"+"Actual");
    }
    public void informacionFase(int dia){
        DatosFase archivo = new DatosFase();
        Fase fase = archivo.leerDatosFase();
        System.out.println("FASE : " + fase.getNombre());
        System.out.println("DIA: " + dia);
        System.out.println("\t"+"\t"+"\t"+"RECOMENDACION");
        InformacionDia(dia);

    }
    public void InformacionDia(int dia){
        switch (dia){
            case 1:
                System.out.println("\t"+"USTED SE DEBE MANTENER CUIDADO ");
                System.out.println("800-10-1104 PARA CONSULTAS SOBRE CORONA VIRUS.");
                break;
            case 2:
                System.out.println("USTED NO ESTA MEJORANDO, DEBE MANTENERSE EN ALERTA");
                System.out.println("800-10-1104 PARA CONSULTAS SOBRE CORONA VIRUS.");
                break;
            case 3:
                System.out.println("USTED SE DEBE REALIZAR PRUEBA UNA DE COVID..!!");
                System.out.println("\t"+"\t"+"_PARA MAS INFORMACION LLAME_");
                System.out.println("800-10-1104 PARA CONSULTAS SOBRE CORONA VIRUS.");
                break;
            case 4:
                System.out.println("¿ YA SE AH REALIZADO UNA PRUEBA DE COVID ?");
                seRealizoPrueba();
                System.out.println("800-10-1104 PARA CONSULTAS SOBRE CORONA VIRUS.");
                break;
            case 5:
                System.out.println("\t"+"DEBE TENER EN CONTACTO A SU DOCTOR");
                break;
            case 6:
                System.out.println("\t"+"DEBE TENER EN CONTACTO A SU DOCTOR");
                System.out.println("\t"+"DEBE TENER EN CONTACTO A SU DOCTOR");
                System.out.println("800-10-1104 PARA CONSULTAS SOBRE CORONA VIRUS.");
                break;
            case 7:
                System.out.println("\t"+"DEBE TENER EN CONTACTO A SU DOCTOR");
                break;
            case 8:
                System.out.println("\t"+"DEBE TENER EN CONTACTO A SU DOCTOR");
                break;
            default:
                System.out.println("Sin Sintomas");
        }
    }
    public void seRealizoPrueba(){
        Scanner sc = new Scanner(System.in);
        System.out.println("1.- Si");
        System.out.println("2.- No");
        int dato = sc.nextInt();
        if(dato == 1){
            positivoNegativo();
        }else {
            System.out.println("ES IMPORTANTE QUE SE REALIZE LA PRUEBA DE COVID");
        }
    }

    public void positivoNegativo(){
        Scanner sc = new Scanner(System.in);
        System.out.println("SU RESULTADO ES :");
        System.out.println("1.- Negativo");
        System.out.println("2.- Positivo");
        int dato = sc.nextInt();
        if (dato == 1){
            System.out.println("RECOMENDAMOS QUE SI PERSISTE LOS SINTOMAS SIGA");
            System.out.println(         "HACIENDO UN SEGUIMIENTO");
        }else{
            System.out.println("DEBE AISLARSE Y LLAMAR SU DOCTOR");
        }
    }


    /**
     * PRIMERA FASE
     * % > 30 && % < 50  = MANTENER CUIDADO
     * % > 50 && % < 65  = REALIZAR PRUEBA DE COVID
     * % > 65 && % < 80  = REALIZAR PRUEBA DE COVID VISITE A UN DOCTOR
     * % > 80 && % < 100 = REALIZAR PRUEBA DE COVID Y MANTENERSE ALERTA
     */
    public void primeraFase(int porcentaje) {
        if (porcentaje >= 30 && porcentaje < 50) {
            System.out.println("MANTENER CUIDADO");
        }
        if (porcentaje >= 50 && porcentaje < 65) {
            System.out.println("REALIZAR PRUEBA DE COVID");
        }
        if (porcentaje >= 65 && porcentaje < 80) {
            System.out.println("REALIZAR PRUEBA DE COVID VISITE A UN DOCTOR");
        }
        if (porcentaje >= 80 && porcentaje < 100) {
            System.out.println("REALIZAR PRUEBA DE COVID,VISITE A UN DOCTOR Y MANTENERSE ALERTA");
        }
    }

}
