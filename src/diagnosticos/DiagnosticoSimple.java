package diagnosticos;

import monitor.FuncionDiagnostico;
import monitor.Registro;
import monitor.Registros;
import monitor.Sintoma;
import monitor.Sintomas;

import java.util.HashMap;
import java.util.Map;

public class DiagnosticoSimple extends FuncionDiagnostico {

    private Map<Sintoma,Integer> pesos;

    public DiagnosticoSimple(Sintomas ls) {
        super(ls);
        pesos = new HashMap<>();
        for (Sintoma s: ls) {
            pesos.put(s,s.peso());
        }
    }

    @Override
    public int diagnostico(Registros registros) {
        int pesoSintomas = 0;
        int res = 0;
        if (!registros.isEmpty()) {
            Registro registro = registros.peek();
            if(registro != null){
                Sintomas sintomas = registro.getSintomas();
                    for (Sintoma s: sintomas) {
                        pesoSintomas += pesos.get(s);
                    }
            }else{
                return 0;
            }
        }
        res = porcentaje(pesos.size(),pesoSintomas);
        return res;
    }

    /* 1.- catidad de sintomas.dat
     * 2.- sumar los pesos
     * 3.- promedio => 400/10 = 40 */

    public int porcentaje(int cantSintomas, int sumPesos){
        int promedio = sumPesos/cantSintomas;
        return promedio;
    }
}
